// PlanView

import React, { Component } from "react";
import PropTypes from "prop-types";
// import ToolbarItems from "../ToolbarItems";
import PlanTreeView from "../PlanTreeView";
import PlanGridView from "../PlanGridView";
import withSizes from "react-sizes";
import appScrollbarWidth from "./../../lib/appScrollbarWidth.js";
import "./styles.css";

class PlanView extends Component {
  constructor(props) {
    super(props);
    this.handleClick = () => false; //this.props.handleClick;
    // this.toolbarItems = this.props.toolbarItems;
    // this.planData = this.props.planData;
    this.windowHeight = this.props.height;
    this.componentMounted = false;
    this.scrollbarWidth = 2;
  }

  adjustElementHeight(windowHeight) {
    if (this.componentMounted) {
      let elem = document.getElementById("planview-container");
      let style = window.getComputedStyle(elem);
      let gridTemplateColumns = style.gridTemplateColumns; //note style neme's change from shiskabab to camelCase
      let columnCount = gridTemplateColumns.split(" ").length; //use this to change toolbar design for different container sizes

      const magicnumber = 20; //this is the window height space not used by the planviewContainer
      let planviewHost = document.getElementById("planview-host");
      let planviewContainer = document.getElementById("planview-container");
      let planviewContainerHeight = windowHeight - magicnumber;
      planviewHost.style.height = planviewContainerHeight - 60 + "px";
      planviewContainer.style.height = planviewContainerHeight + "px";
    }
  }

  componentDidMount() {
    this.componentMounted = true;
    this.adjustElementHeight(this.windowHeight);
    this.scrollbarWidth = appScrollbarWidth();
    window.scrollTo(0, 0);
  }

  render() {
    console.log("height=" + this.props.height + " width=" + this.props.width);
    this.adjustElementHeight(this.props.height);
    return (
      <div id="planview-host">
        <div id="planview-container">
          <div id="datatreebox" className="datatree-box">
            <PlanTreeView />
          </div>
          <div className="planview-toolbar-box">
              <div> <h3>Dummy ToolBar </h3></div>      
          </div>
          <div className="datagrid-box">
            <PlanGridView />
          </div>
        </div>
      </div>
    );
  }
}

PlanView.propTypes = {
  planData: PropTypes.string,
  toolbarItems: PropTypes.array,
  handleClick: PropTypes.func
};

const mapSizesToProps = ({ height, width }) => ({
  height: height,
  width: width
});

// withSizes is used as a HOC to supply window demensions as a prop item to PlanView
// https://www.npmjs.com/package/react-sizes
export default withSizes(mapSizesToProps)(PlanView);
