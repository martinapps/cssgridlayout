import React, { Component } from 'react';
import PlanView from './components/PlanView';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <PlanView />
      </div>
    );
  }
}

export default App;
